package edu.ntnu.texasai.model;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.controller.GameHandController;
import edu.ntnu.texasai.controller.PlayerController;
import edu.ntnu.texasai.controller.PlayerControllerBettingDecisionFifo;
import edu.ntnu.texasai.controller.preflopsim.PlayerControllerPreFlopRoll;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import org.junit.Before;
import org.junit.Test;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class BettingRoundTest {

    private GameHandController gameHandController;
    private PlayerController playerController;

    LinkedList<Player> players;

    public BettingRoundTest() {
    }

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
        this.playerController = injector
                .getInstance(PlayerControllerPreFlopRoll.class);

        players = new LinkedList<>();
        players.add(new Player(1,100,playerController));
        players.add(new Player(2,100,playerController));
    }


    @Test
    public void testPlayerCanBetWhenRoundStart() {
        GameHand gameHand = new GameHand(players,10,20);
        BettingRound bettingRound = new BettingRound(gameHand);
        assertTrue(bettingRound.playerCanBet());
    }

    @Test
    public void testPlayerCanBetWhenP2HasNoActionStart() {
        GameHand gameHand = new GameHand(players,10,20);
        BettingRound bettingRound = new BettingRound(gameHand);
        bettingRound.placeBet(players.iterator().next(), BettingDecision.BettingAction.RAISE,10);
        assertTrue(bettingRound.playerCanBet());
    }

}