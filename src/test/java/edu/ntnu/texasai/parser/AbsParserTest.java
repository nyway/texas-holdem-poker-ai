package edu.ntnu.texasai.parser;

import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.stream.Stream;

public class AbsParserTest {

    @Test
    public void testParsePokerStarHandFromFile() throws Exception {
        AbsParser parser = new AbsParser(new PlayGameControler());

//        URL url = this.getClass().getResource("abs NLH handhq_1-OBFUSCATED.txt");
        URL url = this.getClass().getResource("aa_sample.txt");
        File f=new File(url.toURI());

        try (Stream linesStream = Files.lines( f.toPath() ) ) {
            linesStream.forEach(line -> {
                parser.parseLine((String) line);
            });
        }

    }

    }