package edu.ntnu.texasai.parser;

import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class PokerStarParserTest {

    @Test
    public void testParsePokerStarHand() {
        PokerStarParser pokerStarParser = new PokerStarParser(new PlayGameControler());
        pokerStarParser.parseLine("PokerStars Hand #141572573907: Hold'em No Limit (€0.50/€1.00 EUR) - 2015/10/01 1:35:13 ET");
        pokerStarParser.parseLine("Table 'Leo' 6-max Seat #3 is the button");
        pokerStarParser.parseLine("Seat 2: Gzgarry (€44.25 in chips)");
        pokerStarParser.parseLine("Seat 3: sendmamoney (€100 in chips)");
        pokerStarParser.parseLine("Seat 4: Facundete (€95.85 in chips)");
        pokerStarParser.parseLine("Seat 5: Keijjo69 (€100 in chips)");
        pokerStarParser.parseLine("Facundete: posts small blind €0.50");
        pokerStarParser.parseLine("Keijjo69: posts big blind €1");
        pokerStarParser.parseLine("*** HOLE CARDS ***");
        pokerStarParser.parseLine("Gzgarry: calls €1");
        pokerStarParser.parseLine("sendmamoney: folds");
        pokerStarParser.parseLine("Facundete: calls €0.50");
        pokerStarParser.parseLine("Keijjo69: checks");
        pokerStarParser.parseLine("*** FLOP *** [Js 5c 2s]");
        pokerStarParser.parseLine("Facundete: checks");
        pokerStarParser.parseLine("Keijjo69: checks");
        pokerStarParser.parseLine("Gzgarry: bets €2.86");
        pokerStarParser.parseLine("Facundete: folds");
        pokerStarParser.parseLine("Keijjo69: folds");
        pokerStarParser.parseLine("Uncalled bet (€2.86) returned to Gzgarry");
        pokerStarParser.parseLine("Gzgarry collected €2.86 from pot");
        pokerStarParser.parseLine("Gzgarry: doesn't show hand");
        pokerStarParser.parseLine("*** SUMMARY ***");
//        Total pot €3 | Rake €0.14
//        Board [Js 5c 2s]
//        Seat 2: Gzgarry collected (€2.86)
//        Seat 3: sendmamoney (button) folded before Flop (didn't bet)
//        Seat 4: Facundete (small blind) folded on the Flop
//        Seat 5: Keijjo69 (big blind) folded on the Flop
    }

}
