package edu.ntnu.texasai.controller;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.controller.preflopsim.PlayerControllerPreFlopRoll;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class PlayGameTest {

    private GameHandController gameHandController;
    private PlayerControllerBettingDecisionFifo player1Controller;
    private PlayerControllerBettingDecisionFifo player2Controller;

    public PlayGameTest() {
    }

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
//        this.playerController = injector
//                .getInstance(PlayerControllerPreFlopRoll.class);
    }

    @Test
    public void playRound() {
        LinkedList<Player> players = new LinkedList<>();
        this.player1Controller = new PlayerControllerBettingDecisionFifo();
        this.player2Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);

        gameHandController.playRound(gameHand);
    }

}