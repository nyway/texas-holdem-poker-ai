package edu.ntnu.texasai.controller;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class PlayRoundTest {

    private GameHandController gameHandController;

    public PlayRoundTest() {
    }

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
    }

    @Test
    public void playRoundS1() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();


        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);
        gameHandController.playRound(gameHand);

        player2Controller.pushDecision(BettingDecision.FOLD);
        gameHandController.playRound(gameHand);
    }

    @Test
    public void playRoundS2() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);
        gameHandController.playRound(gameHand);

        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.FOLD);
        gameHandController.playRound(gameHand);
    }

    @Test
    public void playRoundS3() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();


        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);
        gameHandController.playRound(gameHand);

        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);
        gameHandController.playRound(gameHand);

        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);
        gameHandController.playRound(gameHand);

        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);
        gameHandController.playRound(gameHand);
    }

    @Test
    public void playGameS3() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        // preflop
        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);

        // post flop
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // post turn
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // post river
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        gameHandController.play(gameHand);
    }

    @Test
    public void playGameS4() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        // preflop
        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);

        // post flop
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // post turn
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // post river
        player2Controller.pushDecision(BettingDecision.FOLD);

        gameHandController.play(gameHand);
    }


    @Test
    public void playGameSplitPot() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        GameHand gameHand = new GameHand(players,10,20);

        // preflop
        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);

        // post flop
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // post turn
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // post river
        player2Controller.pushDecision(BettingDecision.FOLD);

        gameHandController.play(gameHand);
    }

}