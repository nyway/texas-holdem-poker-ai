package edu.ntnu.texasai.controller;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.CardNumber;
import edu.ntnu.texasai.model.cards.CardSuit;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;

public class PlaySampleGameTest {

    private GameHandController gameHandController;

    public PlaySampleGameTest() {
    }

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
    }

    // https://github.com/alexyz/poker/blob/master/src/pet/hp/impl/PSParser.java

    @Test
    public void playSampleGame() {

    }

}