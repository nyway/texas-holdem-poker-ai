package edu.ntnu.texasai.controller;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static edu.ntnu.texasai.model.BettingDecision.BettingAction.*;
import static edu.ntnu.texasai.model.BettingDecision.RaiseAmount.*;
import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class GameHandControllerTest {

    private GameHandController gameHandController;

    List<BettingDecision> p1BettingDecision;
    List<BettingDecision> p2BettingDecision;

    @Parameterized.Parameters()
    public static Iterable<Object[]> data() throws Throwable
    {
        return asList(new Object[][] {
                {
                    asList(new BettingDecision(CALL),new BettingDecision(RAISE,RAISE_POT)) ,  // p1BettingDecision
                    asList(new BettingDecision(CHECK),new BettingDecision(FOLD)) }            // p2BettingDecision
                }
        );
    }

    public GameHandControllerTest(List<BettingDecision> p1BettingDecision,List<BettingDecision> p2BettingDecision) {
        this.p1BettingDecision = p1BettingDecision;
        this.p2BettingDecision = p2BettingDecision;
    }

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
    }

    @Test
    public void playRound() {
        System.out.println(p1BettingDecision.size());
//        List<Player> players = new LinkedList<>();
//        gameHandController.playRound(new GameHand(players));
    }

}