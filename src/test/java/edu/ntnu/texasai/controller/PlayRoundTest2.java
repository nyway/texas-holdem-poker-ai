package edu.ntnu.texasai.controller;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.CardNumber;
import edu.ntnu.texasai.model.cards.CardSuit;
import edu.ntnu.texasai.model.cards.DeckFifo;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

public class PlayRoundTest2 {

    private GameHandController gameHandController;

    public PlayRoundTest2() {
    }

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
    }

    @Test
    public void playGameSplitPot() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,100,player1Controller));
        players.add(new Player(2,100,player2Controller));
        DeckFifo deck = new DeckFifo();
        GameHand gameHand = new GameHand(players,10,20, deck);

        // p1
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.ACE));
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.KING));

        // p2
        deck.pushCardToTop(new Card(CardSuit.CLUB, CardNumber.ACE));
        deck.pushCardToTop(new Card(CardSuit.CLUB, CardNumber.KING));

        // preflop
        player2Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);

        // flop
        deck.pushCardToTop(new Card(CardSuit.SPADE, CardNumber.NINE));
        deck.pushCardToTop(new Card(CardSuit.CLUB, CardNumber.NINE));
        deck.pushCardToTop(new Card(CardSuit.DIAMOND, CardNumber.NINE));

        // post flop
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // flop
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.NINE));

        // post turn
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // river
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.TEN));

         // post river
        player2Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        gameHandController.play(gameHand);
    }

    @Test
    public void playGameSplitPot2() {
        LinkedList<Player> players = new LinkedList<>();
        PlayerControllerBettingDecisionFifo player1Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player2Controller = new PlayerControllerBettingDecisionFifo();
        PlayerControllerBettingDecisionFifo player3Controller = new PlayerControllerBettingDecisionFifo();

        players.add(new Player(1,20,player1Controller));
        players.add(new Player(2,100,player2Controller));
        players.add(new Player(3,100,player3Controller));
        DeckFifo deck = new DeckFifo();
        GameHand gameHand = new GameHand(players,1,2, deck);

        // p1
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.TEN));
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.SEVEN));

        // p2
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.ACE));
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.KING));

        // p3
        deck.pushCardToTop(new Card(CardSuit.CLUB, CardNumber.ACE));
        deck.pushCardToTop(new Card(CardSuit.CLUB, CardNumber.KING));

        // preflop
        player1Controller.pushDecision(BettingDecision.CALL);
        player2Controller.pushDecision(BettingDecision.FOLD);
        player3Controller.pushDecision(BettingDecision.RAISE_CUSTOM(50));
        player1Controller.pushDecision(BettingDecision.CALL);

        // flop
        deck.pushCardToTop(new Card(CardSuit.SPADE, CardNumber.NINE));
        deck.pushCardToTop(new Card(CardSuit.CLUB, CardNumber.NINE));
        deck.pushCardToTop(new Card(CardSuit.DIAMOND, CardNumber.NINE));

        // post flop
        player3Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // flop
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.NINE));

        // post turn
        player3Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        // river
        deck.pushCardToTop(new Card(CardSuit.HEART, CardNumber.TEN));

        // post river
        player3Controller.pushDecision(BettingDecision.CHECK);
        player1Controller.pushDecision(BettingDecision.CHECK);

        gameHandController.play(gameHand);
    }

// TODO
//-----------------------------------------
//    Game Hand #5
//            -----------------------------------------
//    Player #1(40)$[♦7, ♦9]
//    Player #2(10)$[♠4, ♥9]
//    Player #3(0)$[♣A, ♣6]
//    Player #4(40)$[♣8, ♦4]
//    Player #5(130)$[♥6, ♦Q]
//            -----------------------------------------
//            ---PRE_FLOP (4 active players, 0$)
//    Player #2(10)$[♠7, ♥A]: Small blind 10
//    Player #4(40)$[♣5, ♥Q]: Big blind 20
//    Player #5(130)$[♥3, ♣10]: FOLD 0$
//    Player #1(20)$[♠J, ♥6]: CALL 20$
//    Player #4(20)$[♣5, ♥Q]: CALL 20$
//    Player #1(20)$[♠J, ♥6]: CALL 20$
//    Player #4(20)$[♣5, ♥Q]: CALL 20$
//    Player #1(20)$[♠J, ♥6]: CALL 20$
//    Player #4(20)$[♣5, ♥Q]: CALL 20$
//    Player #1(20)$[♠J, ♥6]: CALL 20$
//    Player #4(20)$[♣5, ♥Q]: CALL 20$




}