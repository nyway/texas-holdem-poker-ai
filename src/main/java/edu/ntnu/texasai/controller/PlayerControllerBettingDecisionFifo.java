package edu.ntnu.texasai.controller;

import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.cards.Card;

import java.util.LinkedList;
import java.util.List;

public class PlayerControllerBettingDecisionFifo extends PlayerController {

    LinkedList<BettingDecision> fifo = new LinkedList<>();

    @Override
    protected BettingDecision decide(Player player, GameHand gameHand, List<Card> cards) {
        if( fifo.isEmpty() ) {
            throw new RuntimeException("no more BettingDecision in fifo for player "+player.getSeat());
        }

        return fifo.removeFirst();
    }

    public void pushDecision(BettingDecision bettingDecision) {
        fifo.add(bettingDecision);
    }
}