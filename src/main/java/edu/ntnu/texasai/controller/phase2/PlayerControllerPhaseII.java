package edu.ntnu.texasai.controller.phase2;

import edu.ntnu.texasai.controller.HandStrengthEvaluator;
import edu.ntnu.texasai.controller.PlayerController;
import edu.ntnu.texasai.controller.PlayerControllerPrePost;
import edu.ntnu.texasai.model.BettingRoundName;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.Table;
import edu.ntnu.texasai.model.opponentmodeling.ContextRaises;

public abstract class PlayerControllerPhaseII extends PlayerControllerPrePost {
    private final HandStrengthEvaluator handStrengthEvaluator;

    protected PlayerControllerPhaseII(final HandStrengthEvaluator handStrengthEvaluator) {
        this.handStrengthEvaluator = handStrengthEvaluator;
    }

    protected double calculateCoefficient(GameHand gameHand, Player player) {
        Table table = gameHand.table;
        double p = this.handStrengthEvaluator.evaluate(player.getHoleCards(), gameHand.getSharedCards(),
                table.getInGamePlayers().size());

        // Decision must depends on the number of players
        p = p * (1 + table.getInGamePlayers().size() / 20);

        // Last round, why not?
        if (gameHand.getBettingRoundName().equals(BettingRoundName.POST_RIVER)) {
            p += 0.3;
        }
        // Lot of raises, be careful
        if (ContextRaises.valueFor(gameHand.getCurrentBettingRound().getNumberOfRaises()).equals(ContextRaises.MANY)) {
            p -= 0.3;
        }

        return p;
    }
}
