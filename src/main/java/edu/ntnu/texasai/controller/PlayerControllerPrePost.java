package edu.ntnu.texasai.controller;

import edu.ntnu.texasai.model.*;
import edu.ntnu.texasai.model.cards.Card;

import java.util.ArrayList;
import java.util.List;

public abstract class PlayerControllerPrePost extends PlayerController {

    @Override
    public BettingDecision decide(Player player, GameHand gameHand,
                                  List<Card> cards) {
        if(gameHand.getBettingRoundName().equals(BettingRoundName.PRE_FLOP)) {
            return decidePreFlop(player,gameHand,cards);
        } else {
            return decideAfterFlop(player,gameHand,cards);
        }
    }

    protected boolean canCheck(GameHand gameHand, Player player) {
        BettingRound bettingRound = gameHand.getCurrentBettingRound();
        return bettingRound.getHighestBet() == bettingRound.getBetForPlayer(player);
    }

    protected abstract BettingDecision decidePreFlop(Player player, GameHand gameHand, List<Card> cards);
    protected abstract BettingDecision decideAfterFlop(Player player, GameHand gameHand, List<Card> cards);

}
