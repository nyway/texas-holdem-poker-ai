package edu.ntnu.texasai.controller;
import static java.lang.Math.min;
import edu.ntnu.texasai.controller.opponentmodeling.OpponentModeler;
import edu.ntnu.texasai.model.*;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.Deck;
import edu.ntnu.texasai.model.gameproperties.GameProperties;
import edu.ntnu.texasai.utils.ILogger;

import javax.inject.Inject;
import java.util.*;

public class GameHandController {
    protected final ILogger logger;
    private final HandPowerRanker handPowerRanker;
    private final GameProperties gameProperties;
    private final StatisticsController statisticsController;
    private final HandStrengthEvaluator handStrengthEvaluator;
    private final OpponentModeler opponentModeler;

    @Inject
    public GameHandController(final ILogger logger,
                              final HandPowerRanker handPowerRanker,
                              final GameProperties gameProperties,
                              final StatisticsController statisticsController,
                              final HandStrengthEvaluator handStrengthEvaluator,
                              final OpponentModeler opponentModeler) {
        this.logger = logger;
        this.handPowerRanker = handPowerRanker;
        this.gameProperties = gameProperties;
        this.statisticsController = statisticsController;
        this.handStrengthEvaluator = handStrengthEvaluator;
        this.opponentModeler = opponentModeler;
    }

    public void play(Game game) {
        logger.log("-----------------------------------------");
        logger.logImportant("Game Hand #" + (game.gameHandsCount() + 1));
        logger.log("-----------------------------------------");
        GameHand gameHand = createGameHand(game);
        Iterator<Player> iterator = gameHand.table.getPlayers().iterator();
        while(iterator.hasNext()) {
            Player player = iterator.next();
            logger.log(player.toString());
        }
        logger.log("-----------------------------------------");

        play(gameHand);
    }

    protected void play(GameHand gameHand) {
        Boolean haveWinner = false;
        while (!gameHand.getBettingRoundName().equals(
                BettingRoundName.POST_RIVER)
                && !haveWinner) {
            haveWinner = playRound(gameHand);
        }

        if (!haveWinner) {
            showDown(gameHand);
        }
    }

    private GameHand createGameHand(Game game) {
        GameHand gameHand = new GameHand(game.getPlayers(),gameProperties.getSmallBlind(),gameProperties.getBigBlind(),new Deck()); // TODO adapt samll/big Blind
        game.addGameHand(gameHand);
        return gameHand;
    }


    /**
     * @param gameHand
     * @return true if we have a winner
     */
    protected Boolean playRound(GameHand gameHand) {
        gameHand.nextRound();

        BettingRound currentBettingRound = gameHand.getCurrentBettingRound();
        logBettingRound(gameHand);

        if (gameHand.getBettingRoundName().equals(BettingRoundName.PRE_FLOP)) {
            takeBlinds(gameHand);
        }

        int numberOfPlayersAtBeginningOfRound = gameHand.table.getActivePlayers().size();
        while (currentBettingRound.playerCanBet()) {
            Player player = gameHand.table.nextPlayer();
            BettingDecision bettingDecision = player.decide(gameHand);
            applyDecision(gameHand, player, bettingDecision);
        }

        //logger.log("Pots: "+gameHand.getPots().toString());

        // Check if we have a winner
        if (gameHand.table.getActivePlayers().size() == 1) {
            Player winner = gameHand.table.currentPlayer();
            winner.addMoney(gameHand.getTotalBets());
            logger.log(winner + ": WIN! +" + gameHand.getTotalBets() + "$");
            return true;
        }

        return false;
    }

    private void logBettingRound(GameHand gameHand) {
        String logMsg = "---" + gameHand.getBettingRoundName();
        logMsg += " (" + gameHand.table.getActivePlayers().size() + " active players, ";
        logMsg += gameHand.table.getInGamePlayers().size() + " players in game, ";
        logMsg += gameHand.getTotalBets() + "$)";
        if (!gameHand.getSharedCards().isEmpty()) {
            logMsg += " " + gameHand.getSharedCards();
        }
        logger.log(logMsg);
    }

    private void takeBlinds(GameHand gameHand) {
        Player smallBlindPlayer = gameHand.table.nextPlayer();
        Player bigBlindPlayer = gameHand.table.nextPlayer();

        int smallBlind = min(gameHand.getSmallBlind(), smallBlindPlayer.getMoney());
        int bigBlind = min(gameHand.getBigBlind(), bigBlindPlayer.getMoney());

        logger.log(smallBlindPlayer + ": Small blind " + smallBlind);
        logger.log(bigBlindPlayer + ": Big blind " + bigBlind);
        // TODO ALL IN ...
        gameHand.getCurrentBettingRound().placeBet(smallBlindPlayer, BettingDecision.BettingAction.RAISE, smallBlind);
        gameHand.getCurrentBettingRound().placeBet(bigBlindPlayer, BettingDecision.BettingAction.RAISE, bigBlind);
    }

    public static int DD = 0;

    private void applyDecision(GameHand gameHand, Player player, BettingDecision bettingDecision) {
        double handStrength = handStrengthEvaluator.evaluate(player.getHoleCards(), gameHand.getSharedCards(),
                gameHand.table.getActivePlayers().size());
        gameHand.applyDecision(player, bettingDecision, gameProperties, handStrength);
        BettingRound bettingRound = gameHand.getCurrentBettingRound();
        DD++;

        logger.log(player + ": " + bettingDecision + " "
                + bettingRound.getBetForPlayer(player) + "$" );
        //logger.log("POTS :"+bettingRound.getPots().toString());
    }

    private LinkedHashMap<Integer,List<Player>> getWinnersByHandPower(GameHand gameHand) {
        Iterable<Player> activePlayers = gameHand.table.getActivePlayers();
        List<Card> sharedCards = gameHand.getSharedCards();

        Map<Integer,List<Player>> winners = new HashMap<>();

        for (Player player : activePlayers) {
            List<Card> mergeCards = new ArrayList<Card>(player.getHoleCards());
            mergeCards.addAll(sharedCards);
            Integer handPower = handPowerRanker.rank(mergeCards).getValue();
            if(!winners.containsKey(handPower)) {
                winners.put(handPower,new ArrayList<>());
            }
            winners.get(handPower).add(player);
        }
        LinkedHashMap<Integer,List<Player>> sortedWinners = new LinkedHashMap<>();
        winners.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> sortedWinners.put(x.getKey(), x.getValue()));
        return sortedWinners;
    }

    private List<Player> getWinnersList(GameHand gameHand) {
        Iterable<Player> activePlayers = gameHand.table.getActivePlayers();
        List<Card> sharedCards = gameHand.getSharedCards();

        HandPower bestHandPower = null;
        List<Player> winners = new ArrayList<Player>();
        for (Player player : activePlayers) {
            List<Card> mergeCards = new ArrayList<Card>(player.getHoleCards());
            mergeCards.addAll(sharedCards);
            HandPower handPower = handPowerRanker.rank(mergeCards);

            logger.log(player + ": " + handPower);

            if (bestHandPower == null || handPower.compareTo(bestHandPower) > 0) {
                winners.clear();
                winners.add(player);
                bestHandPower = handPower;
            } else if (handPower.compareTo(bestHandPower)==0) {
                winners.add(player);
            }
        }
        statisticsController.storeWinners(winners);
        return winners;
    }

    /**
     * TODO https://www.pokerdictionary.net/poker-tips/understanding-side-pots/
     * Method for determining side pot(s) at then end of a betting round when one or more players have gone All-In:
     * - Determine the amount of each All-In bet (if there is more than one)
     * - Select the amount of the smallest All-In bet (if there is more than one)
     * - Deduct that amount from all the bets and add it to the current pot
     * - Close the current pot and move it off to the side (as a side pot)
     * - Start a new current pot
     * Repeat steps 1 to 5 if there are more All-In bets
     * Move the remaining bets to the current pot
     * If a side pot has only one player, the chips are returned to the player
     *
     * @param gameHand
     */
    protected void showDown(GameHand gameHand) {
        logger.log("--- Showdown");
        calculatePotDistribution(gameHand);

        /*
        // Showdown
        LinkedHashMap<HandPower,List<Player>> winners = getWinners(gameHand);
        winners.entrySet();
        for (Map.Entry<HandPower, List<Player>> handPowerList : winners.entrySet()) {

            // Gains
            int gain = gameHand.getTotalBets() / winners.size();
            int modulo = gameHand.getTotalBets() % winners.size();

            for (Player winner : handPowerList.getValue()) {
                int gainAndModulo = gain;
                if (modulo > 0) {
                    gainAndModulo += modulo;
                }
                winner.addMoney(gainAndModulo);
                modulo--;
            }
        }
        */

        // Opponent modeling
        // opponentModeler.save(gameHand);
    }

    public void calculatePotDistribution(GameHand gameHand) {

        // Per rank (single or multiple winners), calculate pot distribution.
        int totalPot = gameHand.getTotalPot();

        Map<Player, Integer> potDivision = new HashMap<Player, Integer>();
        List<Player> activePlayers = gameHand.table.getActivePlayers();

        // TODO start from after dealer for odds

        LinkedHashMap<Integer,List<Player>> winnersMap = getWinnersByHandPower(gameHand);
        for (Map.Entry<Integer, List<Player>> winnersMapEntry : winnersMap.entrySet()) {
            Integer handPower = winnersMapEntry.getKey();
            List<Player> winners = winnersMapEntry.getValue();

            for (Pot pot : gameHand.getPots()) {
                // Determine how many winners share this pot.
                //logger.log(pot.toString()+" nb winner:"+pot.getContributors().size());

                int noOfWinnersInPot = 0;
                for (Player winner : winners) {
                    if (pot.hasContributer(winner)) {
                        noOfWinnersInPot++;
                    }
                }
                if (noOfWinnersInPot > 0) {
                    // Divide pot over winners.
                    int potShare = pot.getValue() / noOfWinnersInPot;
                    for (Player winner : winners) {
                        if (pot.hasContributer(winner)) {
                            Integer oldShare = potDivision.get(winner);
                            if (oldShare != null) {
                                potDivision.put(winner, oldShare + potShare);
                            } else {
                                potDivision.put(winner, potShare);
                            }

                        }
                    }
                    // Determine if we have any odd chips left in the pot.
                    int oddChips = pot.getValue() % noOfWinnersInPot; // TODO cas ou division < small blind

                    // Divide odd chips over winners, starting left of the dealer.
                    while (oddChips > 0) {
                        for (Player activePlayer : activePlayers) {
                            Player winner = activePlayer;
                            Integer oldShare = potDivision.get(winner);
                            if (oldShare != null) {
                                potDivision.put(winner, oldShare + 1);
                                logger.log("[DEBUG] "+winner+" receives an odd chip from the pot.");
                                oddChips--;
                            }
                        }
                    }
                    pot.clear();
                }
            }
        }

        // Divide winnings.
        StringBuilder winnerText = new StringBuilder();
        int totalWon = 0;
        for (Player winner : potDivision.keySet()) {
            int potShare = potDivision.get(winner);
            //winner.win(potShare);
            totalWon += potShare;
            if (winnerText.length() > 0) {
                winnerText.append(", ");
            }
            winnerText.append(String.format("%s wins $ %d", winner, potShare));
            //notifyPlayersUpdated(true);
        }
        winnerText.append('.');
        //notifyMessage(winnerText.toString());
        logger.log(winnerText.toString());

    }


}
