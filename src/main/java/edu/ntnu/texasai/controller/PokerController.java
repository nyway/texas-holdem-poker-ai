package edu.ntnu.texasai.controller;

import edu.ntnu.texasai.utils.Logger;
import edu.ntnu.texasai.model.Game;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.gameproperties.GameProperties;
import edu.ntnu.texasai.utils.ILogger;

import javax.inject.Inject;
import java.util.Iterator;

public class PokerController {
    private final Game game;

    private final ILogger logger;
    private final GameProperties gameProperties;
    private final GameHandController gameHandController;

    @Inject
    public PokerController(final GameHandController gameHandController,
                           final ILogger logger, final GameProperties gameProperties) {
        this.gameHandController = gameHandController;
        this.logger = logger;
        this.gameProperties = gameProperties;

        Logger.setLogger(logger);

        game = new Game(gameProperties.getPlayers());
    }

    public void play() {

        int i = 0;
        while ( i < gameProperties.getNumberOfHands() && (gameProperties.getNbPlayersWithMoney()) > 1) {
            gameHandController.play(game);
            game.setNextDealer();
            i++;
        }

        printFinalStats();
    }

    private void printFinalStats() {
        logger.log("-----------------------------------------");
        logger.log("Statistics");
        logger.log("-----------------------------------------");
        logger.log("Number of hands played: " + game.gameHandsCount());
        for (Player player : game.getPlayers()) {
            logger.log(player.toString() + "(" + player.getPlayerController().toString() + ")" + ": " + player
                    .getMoney() + "$");
        }
    }

}
