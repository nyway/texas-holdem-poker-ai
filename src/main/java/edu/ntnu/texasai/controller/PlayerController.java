package edu.ntnu.texasai.controller;

import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.BettingRound;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.cards.Card;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

public abstract class PlayerController {

    public BettingDecision doDecide(Player player, GameHand gameHand) {
        List<Card> cards = new ArrayList<>();
        cards.addAll(gameHand.getSharedCards());
        cards.addAll(player.getHoleCards());
        return decide(player, gameHand, cards);
    }

    protected abstract BettingDecision decide(Player player, GameHand gameHand, List<Card> cards);

}
