package edu.ntnu.texasai.utils;

public interface ILogger {
    void log(String message);

    void logImportant(String message);
}
