package edu.ntnu.texasai.utils;

public class Logger {

    private static ILogger logger;

    public static ILogger get() {
        if ( logger == null ) {
            System.out.println("No logger injected, using Default ConsoleLogger");
            logger = new ConsoleLogger();
        }
        return logger;
    }

    public static void setLogger(ILogger logger) {
        Logger.logger = logger;
    }



}
