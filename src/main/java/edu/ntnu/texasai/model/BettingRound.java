package edu.ntnu.texasai.model;

import static java.lang.Math.min;
import edu.ntnu.texasai.model.opponentmodeling.ContextAction;
import edu.ntnu.texasai.model.opponentmodeling.ContextInformation;
import edu.ntnu.texasai.model.gameproperties.GameProperties;
import edu.ntnu.texasai.model.BettingDecision.BettingAction;

import java.util.*;

public class BettingRound {

    private final GameHand gameHand;
    private final Map<Player, Bet> playersBet;

    private final List<ContextInformation> contextInformations = new ArrayList<>();

    private int highestBet = 0;

    public BettingRound(GameHand gameHand) {
        this.gameHand = gameHand;
        // init des Bet pour chaque joueur (action null, amount 0)
        playersBet = new HashMap<>();
        for (Player player : gameHand.table.getInGamePlayers()) {
            if( player.getMoney()>0 ){ // TODO voir si > 0 necessaire
                playersBet.put(player,new Bet());
            }
        }
    }

    public void applyDecision(ContextInformation contextInformation, GameProperties gameProperties) {
        ContextAction contextAction = contextInformation.getContextAction();
        BettingDecision bettingDecision = contextAction.getBettingDecision();
        Player player = contextAction.getPlayer();

        // Logger.get().log("Player #"+player.getNumber()+" bettingDecision:"+bettingDecision );
        Bet playerBet = getPlayerBet(player);

        // TODO FOLD CHECK ...
        switch (bettingDecision.getAction()) {
            case FOLD:
                placeFold(player);
                break;
            case CALL:
                placeBet(player, bettingDecision.getAction(), min(highestBet - playerBet.getAmount(), player.getMoney()));
                break;
            case RAISE:
                switch (bettingDecision.getRaiseAmount()) {
                    case RAISE_MIN:
                        placeBet(player, bettingDecision.getAction(), min(highestBet + gameProperties.getBigBlind(), player.getMoney()));
                        break;
                    case RAISE_POT:
                        placeBet(player, bettingDecision.getAction(), min(getCurrentPotSize(), player.getMoney()));
                        break;
                    case RAISE_ALLIN:
                        placeBet(player, bettingDecision.getAction(), player.getMoney());
                        break;
                    case RAISE_CUSTOM:
                        placeBet(player, bettingDecision.getAction(), bettingDecision.getAmount());
                        break;
                }
                break;
        }

        // Don't save context information for pre flop
        // Hand strength is always 0 b/c there's no shared cards
        if (!contextAction.getBettingRoundName().equals(BettingRoundName.PRE_FLOP)) {
            contextInformations.add(contextInformation);
        }
    }

    protected Bet getPlayerBet(Player player) {
        return playersBet.get(player);
    }

    protected Boolean hasPlayerBet(Player player) {
        return playersBet.containsKey(player);
    }


    public Integer getCurrentPotSize() {
        Integer potSize = 0;
        for (Bet bet : playersBet.values()) {
            potSize = potSize + bet.getAmount();
        }
        return potSize;
    }


    private void placeFold(Player player) {
        getPlayerBet(player).setAction(BettingAction.FOLD);
    }

    public void placeBet(Player player, BettingAction action, int bet) {
        Bet previousBet = getPlayerBet(player);
        Integer previousBetAmount = previousBet.getAmount();
        //Logger.get().log("Player #" + player.getNumber() + " placeBet:" + bet);

        if ( bet > player.getMoney()) {
            throw new IllegalArgumentException(
                    "You can't bet more money that you own playerBet:"+previousBetAmount+" bet:"+bet+ " player:"+player + " money:"+player.getMoney());
        }

        player.removeMoney(bet);
        gameHand.contributePot(bet,player);

        if (bet + previousBetAmount > highestBet) {
            highestBet = bet + previousBetAmount;
        } else if (bet + previousBetAmount < highestBet && player.getMoney()!=0 ) {
            throw new IllegalArgumentException(
                    "You can't bet less than the higher bet (bet:"+bet+" highestBet:"+highestBet+") player:"+player+ " money:"+player.getMoney());
        }

        getPlayerBet(player).setAmount(bet + previousBetAmount);
        getPlayerBet(player).setAction(action);
    }

    public int getHighestBet() {
        return highestBet;
    }

    public List<ContextInformation> getContextInformations() {
        return contextInformations;
    }

    public int getBetForPlayer(Player player) {
        if (hasPlayerBet(player)) {
            return getPlayerBet(player).getAmount();
        } else return 0;
    }

    public int getTotalBets() { // TODO duplicate code avec pots size ??
        int totalBets = 0;
        for (Bet bet : playersBet.values()) {
            totalBets += bet.getAmount();
        }
        return totalBets;
    }

    public int getNumberOfRaises() {
        int numberOfRaises = 0;
        for (ContextInformation contextInformation : contextInformations) {
            if (contextInformation.getContextAction().getBettingDecision().equals(BettingDecision.RAISE_MIN)) {
                numberOfRaises++;
            }
        }
        return numberOfRaises;
    }

    public ContextAction getContextActionForPlayer(Player player) {
        for (int i = contextInformations.size(); i > 0; i--) {
            ContextInformation contextInformation = contextInformations.get(i - 1);
            ContextAction contextAction = contextInformation.getContextAction();

            if (contextAction.getPlayer().equals(player)) {
                return contextAction;
            }
        }

        return null;
    }

    /**
     * Fin d'un tour d'enchères (wikipedia)
     * Un tour d'enchères se termine :
     * - soit quand tous les joueurs qui ne se sont pas couchés et qui n'ont pas misé tout leur tapis (donc ceux qui peuvent encore parler) ont parlé et misé le même montant, qu'il soit nul ou non ;
     * - soit quand tous les joueurs, sauf un, se sont couchés.
     *
     * @return
     */
    public boolean playerCanBet() {
        // tous les joueurs sauf 1 se sont couchés
        int playerInGame = 0;
        for (Map.Entry<Player, Bet> playerBetEntry : playersBet.entrySet()) {
            if( playerBetEntry.getValue().getAction() != BettingAction.FOLD && playerBetEntry.getKey().getMoney() > 0 ) {
                playerInGame++;
            }
        }
        if (playerInGame == 1) { return false; }

        // chaque joueur doit avoir pris au moins une decision
        for (Bet bet : playersBet.values()) {
            if(bet.getAction() == null) {
                return true;
            }
        }

        // soit quand tous les joueurs qui ne se sont pas couchés et qui n'ont pas misé tout leur tapis
        // (donc ceux qui peuvent encore parler)
        // ont parlé et misé le même montant, qu'il soit nul ou non
        Integer previousBetAmount = null;
        for (Map.Entry<Player, Bet> playerBetEntry : playersBet.entrySet()) {
            if (playerBetEntry.getValue().getAction() != BettingAction.FOLD && playerBetEntry.getKey().getMoney() > 0) {
                if (previousBetAmount == null) {
                    previousBetAmount = playerBetEntry.getValue().getAmount();
                } else {
                    if (previousBetAmount != playerBetEntry.getValue().getAmount()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
