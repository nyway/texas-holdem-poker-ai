package edu.ntnu.texasai.model.cards;

import java.util.List;

public interface IDeck {

    Card removeTopCard();

    boolean removeCard(Card card);
}
