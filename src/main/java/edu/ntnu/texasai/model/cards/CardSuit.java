package edu.ntnu.texasai.model.cards;

public enum CardSuit {
    SPADE("\u2660"),
    HEART("\u2665"),
    CLUB("\u2663"),
    DIAMOND("\u2666");

    private final String symbol;

    private CardSuit(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }

    public static CardSuit fromChar(char shcd) {
        switch (shcd) {
            case 's':
                return SPADE;
            case 'h':
                return HEART;
            case 'c':
                return CLUB;
            case 'd':
                return DIAMOND;
        }
        return null;
    }
}
