package edu.ntnu.texasai.model.cards;

import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.Deck;

import java.util.LinkedList;
import java.util.List;

public class DeckFifo extends Deck {

    private LinkedList<Card> predefinedCards = new LinkedList<>();

    public DeckFifo() {
    }

    @Override
    public Card removeTopCard() {
        if( !predefinedCards.isEmpty() ) {
            return predefinedCards.removeFirst();
        }
        return super.removeTopCard();
    }

    public void pushCardToTop(Card card) {
        removeCard(card);
        predefinedCards.add(card);
    }

}
