package edu.ntnu.texasai.model;

import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.Deck;
import edu.ntnu.texasai.model.cards.IDeck;
import edu.ntnu.texasai.model.opponentmodeling.ContextAction;
import edu.ntnu.texasai.model.opponentmodeling.ContextInformation;
import edu.ntnu.texasai.model.gameproperties.GameProperties;

import java.util.*;

public class GameHand {

//r    private final Deque<Player> players;
//r    private final Deque<Player> activePlayers;
    public final Table table;

    private final IDeck deck;
    private final List<Card> sharedCards = new ArrayList<Card>();
    private final List<BettingRound> bettingRounds = new ArrayList<BettingRound>();
    private final List<Pot> pots;

    private Boolean hasRemoved = true;

    private final int smallBlind;
    private final int bigBlind;

    public GameHand(LinkedList<Player> players, int smallBlind, int bigBlind) {
        this(players,smallBlind,bigBlind,new Deck());
    }

    public GameHand(LinkedList<Player> players, int smallBlind, int bigBlind, IDeck deck) {
        this.table = new Table(6,players);
//r        this.players = new LinkedList<Player>(players);
//r        this.activePlayers = new LinkedList<Player>(players);
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.deck = deck;
        this.pots = new ArrayList<Pot>();
    }

    public BettingRound nextRound() {
        table.newRound();
        BettingRound bettingRound = new BettingRound(this);
        bettingRounds.add(bettingRound);

        if (getBettingRoundName().equals(BettingRoundName.PRE_FLOP)) {
            dealHoleCards();
        } else if (getBettingRoundName().equals(BettingRoundName.POST_FLOP)) {
            dealFlopCards();
        } else {
            dealSharedCard();
        }
        return bettingRound;
    }

    public int getTotalBets() {
        int totalBets = 0;
        for (BettingRound bettingRound : bettingRounds) {
            totalBets += bettingRound.getTotalBets();
        }
        return totalBets;
    }

    public BettingRoundName getBettingRoundName() {
        return BettingRoundName.fromRoundNumber(bettingRounds.size());
    }

    public List<Card> getSharedCards() {
        return sharedCards;
    }

    public BettingRound getCurrentBettingRound() {
        return bettingRounds.get(bettingRounds.size() - 1);
    }

    public List<BettingRound> getBettingRounds() {
        return bettingRounds;
    }

    public int getSmallBlind() {
        return smallBlind;
    }

    public int getBigBlind() {
        return bigBlind;
    }

//    public void removeCurrentPlayer() {
//        activePlayers.remove(players.getFirst());
//        players.removeFirst();
//        hasRemoved = true;
//    }

    protected void dealHoleCards() {
        for (Player player : table.getInGamePlayers()) {
            Card hole1 = deck.removeTopCard();
            Card hole2 = deck.removeTopCard();
            player.setHoleCards(hole1, hole2);
        }
    }

    private void dealFlopCards() {
        sharedCards.add(deck.removeTopCard());
        sharedCards.add(deck.removeTopCard());
        sharedCards.add(deck.removeTopCard());
    }

    private void dealSharedCard() {
        sharedCards.add(deck.removeTopCard());
    }

    public void applyDecision(Player player, BettingDecision bettingDecision, GameProperties gameProperties,
                              double handStrength) {

        BettingRound currentBettingRound = getCurrentBettingRound();
        double potOdds = calculatePotOdds(player);
        ContextAction contextAction = new ContextAction(player, bettingDecision, getBettingRoundName(),
                currentBettingRound.getNumberOfRaises(),
                table.getInGamePlayers().size(), potOdds);
        ContextInformation contextInformation = new ContextInformation(contextAction, handStrength);

        currentBettingRound.applyDecision(contextInformation, gameProperties);


        if (bettingDecision.equals(BettingDecision.FOLD)) {
            table.setCurrentPlayerOut();
        }
    }

    public double calculatePotOdds(Player player) {
        BettingRound currentBettingRound = getCurrentBettingRound();
        int amountNeededToCall = currentBettingRound.getHighestBet() - currentBettingRound.getBetForPlayer(player);
        return (double) amountNeededToCall / (amountNeededToCall + getTotalBets());
    }

    protected IDeck getDeck() {
        return deck;
    }

    public void contributePot(int amount, Player player) {
        for (Pot pot : pots) {
            if (!pot.hasContributer(player)) {
                int potBet = pot.getBet();
                if (amount >= potBet) {
                    // Regular call, bet or raise.
                    pot.addContributer(player);
                    amount -= pot.getBet();
                } else {
                    // Partial call (all-in); redistribute pots.
                    pots.add(pot.split(player, amount));
                    amount = 0;
                }
            }
            if (amount <= 0) {
                break;
            }
        }
        if (amount > 0) {
            Pot pot = new Pot(amount);
            pot.addContributer(player);
            pots.add(pot);
        }
    }

    public List<Pot> getPots() {
        return pots;
    }

    public int getTotalPot() {
        int totalPot = 0;
        for (Pot pot : pots) {
            totalPot += pot.getValue();
        }
        return totalPot;
    }

}
