package edu.ntnu.texasai.model;

public class BettingDecision {

    public static BettingDecision FOLD = new BettingDecision(BettingAction.FOLD);
    public static BettingDecision CHECK = new BettingDecision(BettingAction.CHECK);
    public static BettingDecision CALL = new BettingDecision(BettingAction.CALL);
    public static BettingDecision RAISE_MIN = new BettingDecision(BettingAction.RAISE, RaiseAmount.RAISE_MIN);
    public static BettingDecision RAISE_POT = new BettingDecision(BettingAction.RAISE, RaiseAmount.RAISE_POT);
    public static BettingDecision RAISE_ALLIN = new BettingDecision(BettingAction.RAISE, RaiseAmount.RAISE_ALLIN);
    public static BettingDecision RAISE_CUSTOM(int amount) {
        return new BettingDecision(BettingAction.RAISE, RaiseAmount.RAISE_CUSTOM, amount);
    }

    public enum BettingAction {
        CHECK, CALL, FOLD, RAISE
    }

    public enum RaiseAmount {
        RAISE_MIN, RAISE_POT, RAISE_ALLIN, RAISE_CUSTOM
    }

    private Integer amount;
    private final BettingAction bettingAction;
    private final RaiseAmount raiseAmount;

    public BettingDecision(BettingAction bettingAction) {
        this.bettingAction = bettingAction;
        this.raiseAmount = null;
    }

    public BettingDecision(BettingAction bettingAction, Integer amount) {
        this.bettingAction = bettingAction;
        this.raiseAmount = null;
        this.amount = amount;
    }

    public BettingDecision(BettingAction bettingAction, RaiseAmount raiseAmount) {
        this.bettingAction = bettingAction;
        this.raiseAmount = raiseAmount;
    }

    public BettingDecision(BettingAction bettingAction, RaiseAmount raiseAmount, Integer amount) {
        this.bettingAction = bettingAction;
        this.raiseAmount = raiseAmount;
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BettingAction getBettingAction() {
        return bettingAction;
    }

    public BettingAction getAction() {
        return getBettingAction();
    }

    public RaiseAmount getRaiseAmount() {
        return raiseAmount;
    }

    @Override
    public String toString() {
        return getBettingAction().toString();
    }
}
