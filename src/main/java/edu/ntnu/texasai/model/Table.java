package edu.ntnu.texasai.model;

import java.util.*;

public class Table {

    public final int maxSeats;
    public int dealerSeat;
    public int currentPlayerSeat = -1;
    public Map<Integer,Player> playersMap = new HashMap();
    private List<Player> activePlayers;
    private List<Player> inGamePlayers;

    public Table(int maxSeats, List<Player> players) { // TODO remove this
        this(maxSeats,1,players);
    }

    public Table(int maxSeats, int dealerSeat, List<Player> players) {
        this.maxSeats = maxSeats;
        this.dealerSeat = dealerSeat;
        initActivePlayers(players);
        initInGamePlayers(players);
        for (Player player : players) {
            playersMap.put(player.getSeat(),player);
        }
    }

    private void initActivePlayers(Collection<Player> players) {
        activePlayers = new LinkedList<>();
        for (Player player : players) {
            if( player.getMoney() > 0) activePlayers.add(player);
        }
    }

    private void initInGamePlayers(Collection<Player> players) {
        inGamePlayers = new LinkedList<>();
        for (Player player : players) {
            if( player.getMoney() > 0) inGamePlayers.add(player);
        }
    }

    public Collection<Player> getPlayers() {
        return playersMap.values();
    }

    public void addPlayer(Player p) {
        // TODO check money > 0
        playersMap.put(p.getSeat(),p);
        activePlayers.add(p);
        inGamePlayers.add(p);
    }

    public void setPlayerOut(Player p) {
        inGamePlayers.remove(p);
        activePlayers.remove(p);
    }

    public void setCurrentPlayerOut() {
        setPlayerOut(currentPlayer());
    }

    public Player nextPlayer() {
        if( currentPlayerSeat < 0) {
            currentPlayerSeat = dealerSeat;
        }
        // inc
        currentPlayerSeat = (currentPlayerSeat + 1) % maxSeats;
        while( !inGamePlayers.contains(playersMap.get(currentPlayerSeat)) || playersMap.get(currentPlayerSeat).getMoney() == 0 ) {
            currentPlayerSeat = (currentPlayerSeat + 1) % maxSeats;
        }
        return playersMap.get(currentPlayerSeat);
    }

    public Player currentPlayer() {
        return playersMap.get(currentPlayerSeat);
    }

    /**
     * return still active players to decide some action (bet / fold ...)
     * @return
     */
    public List<Player> getInGamePlayers() {
        return inGamePlayers;
    }

    /**
     * @return still active player for the pot (in game + allin player)
     */
    public List<Player> getActivePlayers() {
        return activePlayers;
    }

    /**
     * dealer++
     * currentPlayer = afterDealer
     * all player with money are active / in game
     */
    public void newRound() {
        currentPlayerSeat=-1;
    }
}
