package edu.ntnu.texasai.model;

import edu.ntnu.texasai.controller.PlayerController;
import edu.ntnu.texasai.model.cards.Card;

import java.util.Arrays;
import java.util.List;

public class Player {
    private final int seat;
    private final PlayerController playerController;
    private int money;
    private List<Card> holeCards;

    public Player(int number, int initialMoney,
            PlayerController playerController) {
        this.seat = number;
        this.money = initialMoney;
        this.playerController = playerController;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Player)) {
            return false;
        }

        Player otherPlayer = (Player) o;

        return seat == otherPlayer.seat;
    }

    @Override
    public int hashCode() {
        return seat;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Player #");
        stringBuilder.append(getSeat()+ "("+money+")$" );

        if (holeCards != null) {
            stringBuilder.append(holeCards.toString());
        }

        return stringBuilder.toString();
    }

    public BettingDecision decide(GameHand gameHand) {
        return playerController.doDecide(this, gameHand);
    }

    public int getSeat() {
        return seat;
    }

    public int getMoney() {
        return money;
    }

    public void removeMoney(int amount) {
        money -= amount;
    }

    public void addMoney(int amount) {
        money += amount;
    }

    public void setHoleCards(Card hole1, Card hole2) {
        holeCards = Arrays.asList(hole1, hole2);
    }

    public List<Card> getHoleCards() {
        return holeCards;
    }

    public PlayerController getPlayerController() {
        return playerController;
    }
}
