package edu.ntnu.texasai.model.gameproperties;

import edu.ntnu.texasai.model.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class GameProperties {
    private final int smallBlind;
    private final int bigBlind;
    private final int initialMoney;
    private final int numberOfHands;
    private final LinkedList<Player> players = new LinkedList<Player>();

    protected GameProperties(int numberOfHands, int initialMoney, int bigBlind, int smallBlind) {
        this.numberOfHands = numberOfHands;
        this.initialMoney = initialMoney;
        this.bigBlind = bigBlind;
        this.smallBlind = smallBlind;
    }

    public int getSmallBlind() {
        return smallBlind;
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public int getInitialMoney() {
        return initialMoney;
    }

    public int getNumberOfHands() {
        return numberOfHands;
    }

    public LinkedList<Player> getPlayers() {
        return players;
    }

    protected void addPlayer(Player player){
        players.add(player);
    }

    public int getNbPlayersWithMoney() {
        int nbPlayerWithMoney = 0;
        Iterator<Player> it = players.iterator();
        while (it.hasNext()) {
            if(it.next().getMoney()>0) {
                nbPlayerWithMoney ++;
            }
        }
        return nbPlayerWithMoney;
    }

}
