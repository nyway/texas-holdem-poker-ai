package edu.ntnu.texasai.model.preflopsim;

import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.Deck;
import edu.ntnu.texasai.model.cards.EquivalenceClass;
import edu.ntnu.texasai.model.cards.IDeck;
import edu.ntnu.texasai.model.gameproperties.GameProperties;

import java.util.LinkedList;
import java.util.List;

public class GameHandPreFlopRoll extends GameHand {

    private final EquivalenceClass equivalenceClass;

    public GameHandPreFlopRoll(final LinkedList<Player> players, int smallBlind, int bigBlind, final EquivalenceClass equivalenceClass) {
        super(players,smallBlind,bigBlind,new Deck());
        this.equivalenceClass = equivalenceClass;
    }

    /**
     * Deals the hole cards. The prospective of the simulation is player0's one,
     * so players0's hole cards are the same of equivalence cards, while the
     * other players receive random cards form the top of the deck.
     */
    @Override
    protected void dealHoleCards() {
        IDeck deck = getDeck();
        List<Card> holeCards = this.equivalenceClass.equivalence2cards();
        Card hole1 = holeCards.get(0);
        Card hole2 = holeCards.get(1);
        for (Player player : this.table.getPlayers()) {
            if (player.getSeat() == 1) {
                player.setHoleCards(hole1, hole2);
                deck.removeCard(hole1);
                deck.removeCard(hole2);
            }
        }

        // other players card are random
        for (Player player : this.table.getPlayers()) {
            if (player.getSeat() != 1) {
                hole1 = deck.removeTopCard();
                hole2 = deck.removeTopCard();
                player.setHoleCards(hole1, hole2);
            }
        }
    }
}
