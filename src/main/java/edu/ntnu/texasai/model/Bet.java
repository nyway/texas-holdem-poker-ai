package edu.ntnu.texasai.model;

import edu.ntnu.texasai.model.BettingDecision.BettingAction;

public class Bet {

    private BettingAction action = null;
    private Integer amount = 0;

    public Bet() {
    }

    public BettingAction getAction() {
        return action;
    }

    public void setAction(BettingAction action) {
        this.action = action;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
