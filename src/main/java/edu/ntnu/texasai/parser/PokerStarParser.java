package edu.ntnu.texasai.parser;

import static edu.ntnu.texasai.model.BettingDecision.BettingAction;

import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.CardNumber;
import edu.ntnu.texasai.model.cards.CardSuit;
import edu.ntnu.texasai.utils.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PokerStarParser {

    final PlayGameControler gameControler;
    List<String> playersName = new ArrayList<>();

    public PokerStarParser(PlayGameControler gameControler) {
        this.gameControler = gameControler;
    }

    public void parseLine (String line) {
        line = line.trim();
        line = line.replace(" - ",": ");
        if(line.length()==0) return;
        Logger.get().log("|**|"+line+"|**|");

        if (line.startsWith("PokerStars ")) {
            parsePokerStarsHand(line);
        } else if ( line.startsWith("Table ") ){
            parseTable(line);
        } else if ( line.startsWith("Seat ") ){
            parseSeat(line);
        } else if ( line.contains(":") && playersName.contains(extractPlayerName(line)) ) {
            parsePlayerAction(line);
        } else if ( line.equals("*** HOLE CARDS ***") ) {
            gameControler.holeCards();
        } else if ( line.startsWith("*** FLOP ***") ) {
            parseFlop(line);
        }
    }

    // *** FLOP *** [Js 5c 2s]
    private static final Pattern flopPattern = Pattern.compile("\\*\\*\\* FLOP \\*\\*\\* \\[(\\w\\w) (\\w\\w) (\\w\\w)\\]");
    private void parseFlop(String line) {
        Matcher m = flopPattern.matcher(line);
        if (m.find()) {
            Logger.get().log("flop: "+m.group(1)+","+m.group(2)+","+m.group(3));
            gameControler.setFlop(parseCard(m.group(1)),parseCard(m.group(2)),parseCard(m.group(3)));
        }
    }

    private Card parseCard(String cardString) {
        return new Card(CardSuit.fromChar(cardString.charAt(1)), CardNumber.fromChar(cardString.charAt(1)));
    }

    private void parsePlayerAction(String line) {
        String playerName = extractPlayerName(line);
        String action = extractAction(line);
        Logger.get().log(playerName+" action:"+ action);
        // TODO assert post

        // Gzgarry: calls €1
        if( action.startsWith("calls") ) {
            gameControler.setNextPlayerAction(playerName, new BettingDecision(BettingAction.CALL));
        } else if ( action.startsWith("checks") ) {
            gameControler.setNextPlayerAction(playerName, new BettingDecision(BettingAction.CHECK));
        //} else if ( action.startsWith("") ) {
        //} else if ( action.startsWith("") ) {
        //} else if ( action.startsWith("") ) {
        }
    }

    private String extractPlayerName(String line) {
        return line.substring(0,line.indexOf(":"));
    }

    private String extractAction(String line) {
        return line.substring(line.indexOf(":")+2);
    }

    // Seat 2: Gzgarry (€44.25 in chips)"
    private static final Pattern seatPattern = Pattern.compile("Seat (\\d): (.*) \\([€$](\\d*?\\.?\\d*) in chips\\)");
    private void parseSeat(String line) {
        Matcher m = seatPattern.matcher(line);
        if (m.find()) {
            playersName.add(m.group(2));
            gameControler.addPlayer(m.group(2), Integer.parseInt(m.group(1)),Float.parseFloat(m.group(3)));
        }
    }

    // PokerStars Hand #141572573907: Hold'em No Limit (€0.50/€1.00 EUR) - 2015/10/01 1:35:13 ET
    private static final Pattern handPattern = Pattern.compile("€(\\d*?\\.?\\d*)");
    void parsePokerStarsHand(String line) {
        Matcher m = handPattern.matcher(line);
        if (m.find()) {
            float sb= Float.parseFloat(m.group(1));
//            Logger.get().log("sb="+sb);
            gameControler.setSmallBlind(sb);
        }
        if (m.find()) {
            float bb= Float.parseFloat(m.group(1));
//            Logger.get().log("bb="+bb);
            gameControler.setBigBlind(bb);
        }
    }

    // Table 'Leo' 6-max Seat #3 is the button
    private static final Pattern tablePattern = Pattern.compile("Table '.*' (\\d*)-max Seat #(\\d*) is the button");
    private void parseTable(String line) {
        Matcher m = tablePattern.matcher(line);
        if (m.find()) {
            gameControler.setMaxSeat(Integer.parseInt(m.group(1)));
            gameControler.setButton(Integer.parseInt(m.group(2)));
        }
    }

}
