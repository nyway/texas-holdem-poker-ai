package edu.ntnu.texasai.parser;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.controller.GameHandController;
import edu.ntnu.texasai.controller.PlayerController;
import edu.ntnu.texasai.controller.PlayerControllerBettingDecisionFifo;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.BettingDecision;
import edu.ntnu.texasai.model.GameHand;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.cards.Card;
import edu.ntnu.texasai.model.cards.DeckFifo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PlayGameControler {

    int smallBlind;
    int bigBlind;
    int maxSeat;
    DeckFifo deck = new DeckFifo();

    private LinkedList<Player> players = new LinkedList<>();
    private GameHand gameHand;
    private final GameHandController gameHandController;

    public PlayGameControler() {
        Injector injector = Guice.createInjector(new TexasModule());
        this.gameHandController = injector.getInstance(GameHandController.class);
    }

    public int getMaxSeat() {
        return maxSeat;
    }

    public void setMaxSeat(int maxSeat) {
        this.maxSeat = maxSeat;
    }

    public int getButton() {
        return button;
    }

    public void setButton(int button) {
        this.button = button;
    }

    int button;

    public int getSmallBlind() {
        return smallBlind;
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(int bigBlind) {
        this.bigBlind = bigBlind;
    }

    public void setSmallBlind(int smallBlind) {
        this.smallBlind = smallBlind;
    }

    public void setSmallBlind(float smallBlind) {
        this.smallBlind = Math.round(smallBlind*100);
    }

    public void setBigBlind(float bigBlind) {
        this.bigBlind = Math.round(bigBlind*100);
    }

    public void addPlayer(String playerName, int seat, float chips) {
        addPlayer(playerName,seat, Math.round(chips*100));
    }

    public void addPlayer(String playerName, int seat, int chips) {
        System.out.println(playerName +" on seat:"+seat+" chips:"+chips);
        PlayerController playerController = new PlayerControllerBettingDecisionFifo();
        players.add(new Player(seat,chips,playerController));

        if(seat>maxSeat) maxSeat = seat;
    }

    public void initGame() {
        this.gameHand = new GameHand(players,smallBlind,bigBlind,deck);
    }

    public void holeCards() {
        initGame();
    }

    public void setNextPlayerAction(String playerName, BettingDecision bettingDecision) {
    }

    public void setFlop(Card card1, Card card2, Card card3) {
//        deck.pushCardToTop(card1);
//        deck.pushCardToTop(card2);
//        deck.pushCardToTop(card3);
    }
}
