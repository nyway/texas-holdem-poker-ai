package edu.ntnu.texasai;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ntnu.texasai.controller.GameHandController;
import edu.ntnu.texasai.controller.PokerController;
import edu.ntnu.texasai.controller.phase1.PlayerControllerPhaseIBluff;
import edu.ntnu.texasai.controller.phase1.PlayerControllerPhaseINormal;
import edu.ntnu.texasai.dependencyinjection.GamePropertiesParameter;
import edu.ntnu.texasai.dependencyinjection.LogLevel;
import edu.ntnu.texasai.dependencyinjection.TexasModule;
import edu.ntnu.texasai.model.Player;
import edu.ntnu.texasai.model.gameproperties.GameProperties;

import javax.inject.Inject;

public class Play {

    public static void main(String[] args) {
        String gameP = "phase1";
        if(args.length == 1){
            gameP = args[0];
        }

        for(int i=0;i<100;i++) {
            GameHandController.DD = 0;
            Injector injector = Guice.createInjector(new TexasModule(LogLevel.ALL, GamePropertiesParameter.fromString(gameP)));
            PokerController pokerController = injector.getInstance(PokerController.class);
            pokerController.play();
        }
    }
}
